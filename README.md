# Loan api demo

## #Getting started

### Technologies
I chose to use Lumen because I work with the framework on a daily basis, so it makes a lot easier.

### Prerequisites

1. git
2. docker

### Installation

Change *hosts* file to point the domain <u>*api.aspire.com*</u> to localhost

Build and bring the container online

``` bash
cd docker
docker-compose up -d
```
Using postman for testing the api. Postman collection and environment are in the postman folder.

Connect to test url: http://api.asprie.com

**<u>Note for not-first-time installation</u>**: the process of one-step installation will not always run smoothly in further setup due to redundant file existence, database table existence, environment configuration...

#### <u>Run worker locally</u>

Go inside docker container of each service

```
docker exec -it [docker-container-name] bash
```




