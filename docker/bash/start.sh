#!/bin/bash
echo "authentication bash script running"
cd /var/www/html/
composer install

php artisan migrate --seed

php artisan passport:install

# Execute the default command, ie php-fpm
exec "php-fpm"
