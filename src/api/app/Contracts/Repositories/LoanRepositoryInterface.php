<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LoanRepository.
 *
 * @package namespace App\Repositories;
 */
interface LoanRepositoryInterface extends RepositoryInterface
{
    //
}
