<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RepaymentRepository.
 *
 * @package namespace App\Repositories;
 */
interface RepaymentRepositoryInterface extends RepositoryInterface
{
    //
}
