<?php


namespace App\Http\Controllers;


use App\Contracts\Repositories\LoanRepositoryInterface;
use App\Http\Requests\Loans\ApproveLoanRequest;
use App\Http\Requests\Loans\CreateLoanRequest;
use App\Http\Requests\Loans\UpdateLoanRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoanController extends Controller
{
    private const TERM = 7; // weekly
    /**
     * @var LoanRepositoryInterface
     */
    private LoanRepositoryInterface $repository;

    /**
     * RoleController constructor.
     * @param LoanRepositoryInterface $repository
     */
    public function __construct(LoanRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $parameters = $request->all();
        $parameters['limit'] = $parameters['limit'] ?? config('repository.pagination.limit', 15);
        $parameters['user_id'] = Auth::id();
        $data = $this->repository->getAllRecords($parameters);
//        return \response()->json($data);
        return \ResponseFormatter::send($data);
    }

    public function detail($id)
    {
        $record = $this->repository->find($id);
        return \ResponseFormatter::send($record);
    }

    public function create(CreateLoanRequest $request)
    {
        $parameters = $request->all();
        $parameters['user_id'] = Auth::id();
        $parameters['date_applied'] = Carbon::now();
        $parameters['date_end'] = Carbon::today()->addDays($parameters['duration']);
        $parameters['pay_per_term'] = $parameters['total_to_pay'] / ceil($parameters['duration']/self::TERM);
        $record = $this->repository->create($parameters);
        return \ResponseFormatter::send($record);
    }

    public function update($id, UpdateLoanRequest $request)
    {
        $parameters = $request->all();
        $parameters['date_applied'] = Carbon::now();
        $parameters['date_end'] = Carbon::today()->addDays($parameters['duration']);
        $parameters['pay_per_term'] = $parameters['total_to_pay'] / ceil($parameters['duration']/self::TERM);
        $record = $this->repository->update($parameters, $id);
        return \ResponseFormatter::send($record);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
        return \ResponseFormatter::send([], Response::HTTP_OK);
    }

    public function approveLoan($id)
    {
        $this->repository->approveLoan($id);
        return \ResponseFormatter::send([], Response::HTTP_OK);
    }
}
