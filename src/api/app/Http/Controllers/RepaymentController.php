<?php


namespace App\Http\Controllers;

use App\Contracts\Repositories\LoanRepositoryInterface;
use App\Contracts\Repositories\RepaymentRepositoryInterface;
use App\Http\Requests\Repayments\CreateRepaymentRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class RepaymentController extends Controller
{
    /**
     * @var RepaymentRepositoryInterface
     */
    private RepaymentRepositoryInterface $repaymentRepository;

    /**
     * @var LoanRepositoryInterface
     */
    private LoanRepositoryInterface $loanRepository;

    /**
     * RepaymentController constructor.
     * @param RepaymentRepositoryInterface $repaymentRepository
     * @param LoanRepositoryInterface $loanRepository
     */
    public function __construct(RepaymentRepositoryInterface $repaymentRepository, LoanRepositoryInterface $loanRepository)
    {
        $this->repaymentRepository = $repaymentRepository;
        $this->loanRepository = $loanRepository;
    }

    public function index(Request $request)
    {
        $parameters = $request->all();
        $parameters['limit'] = $parameters['limit'] ?? config('repository.pagination.limit', 15);
        $data = $this->repaymentRepository->getAllRecords($parameters);
        return \ResponseFormatter::send($data);
    }

    public function detail($id)
    {
        $record = $this->repaymentRepository->find($id);
        return \ResponseFormatter::send($record);
    }

    public function create(CreateRepaymentRequest $request)
    {
        $parameters = $request->all();
        $loan = $this->loanRepository->find($parameters['loan_id']);

        if ($loan->approved == false) {
            return \ResponseFormatter::send([], Response::HTTP_NOT_ACCEPTABLE, ['message' => 'This loan has not been approved']);
        }
        if ($loan->status === 'paid') {
            return \ResponseFormatter::send([], Response::HTTP_NOT_ACCEPTABLE, ['message' => 'This loan has fully been paid.']);
        }

        $repayment = $this->repaymentRepository->create($parameters);
        return \ResponseFormatter::send($repayment);
    }


    public function approveRepayment($id)
    {
        $repayment = $this->repaymentRepository->find($id);

        if ($repayment->approved == true) {
            return \ResponseFormatter::send([], Response::HTTP_NOT_ACCEPTABLE, ['message' => 'This repayment has been approved']);
        }
        $this->loanRepository->updateByRepayment($repayment->loan_id, $repayment->amount);

        $repayment->approved = true;
        $repayment->save();
        return \ResponseFormatter::send([], Response::HTTP_OK);
    }
}
