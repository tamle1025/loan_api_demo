<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\UserRepositoryInterface;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\LoginRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $repository;

    /**
     * UsersController constructor.
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param LoginRequest $request
     * @return mixed
     */
    public function login(LoginRequest $request)
    {

        try {
            $credentials = $request->all();
            $users = $this->repository->findWhere(['email' => $credentials['email']]);

            if (count($users)) {
                $user = $users->first();

                if (Hash::check(($credentials['password']), $user->password)) {
                    $accessToken = $user->createToken('Access Token')->accessToken;
                    return \ResponseFormatter::send(['access_token' => $accessToken]);
                }
            }
            return \ResponseFormatter::send([], Response::HTTP_UNPROCESSABLE_ENTITY, ['message' => "Login failed!"]);
        } catch (\Exception $e) {
            return \ResponseFormatter::send([], Response::HTTP_UNPROCESSABLE_ENTITY, ['message' => "Login failed!"]);
        }
    }


    /**
     * Register a new user.
     *
     * @param Request $request
     * @return Response
     */
    public function register(CreateUserRequest $request)
    {
        $userData = $request->all();
        $user = $this->repository->create($userData);
        return \ResponseFormatter::send($user);
    }
}
