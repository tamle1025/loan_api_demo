<?php


namespace App\Http\Requests\Loans;

use App\Http\Requests\FormRequest;

class CreateLoanRequest extends FormRequest
{
    /**
     * @return string[]
     */
    protected function rules()
    {
        return [
            "amount" => "required|regex:/^\d+(\.\d{1,2})?$/",
            "total_to_pay" => "required|regex:/^\d+(\.\d{1,2})?$/",
            "duration" => "required|int"
        ];
    }
}
