<?php


namespace App\Http\Requests\Repayments;

use App\Http\Requests\FormRequest;

class CreateRepaymentRequest extends FormRequest
{
    /**
     * @return string[]
     */
    protected function rules()
    {
        return [
            "loan_id" => "required|int",
            "amount" => "required|regex:/^\d+(\.\d{1,2})?$/",

        ];
    }
}
