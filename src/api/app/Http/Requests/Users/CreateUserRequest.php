<?php


namespace App\Http\Requests\Users;

use App\Http\Requests\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * @return string[]
     */
    protected function rules()
    {
        return [
            "email" => "required|email|unique:users",
            "password" => "required|string",
            "name" => "required|string",
        ];
    }
}
