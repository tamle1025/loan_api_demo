<?php


namespace App\Http\Requests\Users;

use App\Http\Requests\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * @return string[]
     */
    protected function rules()
    {
        return [
            "email" => "required|email",
            "password" => "required|string",
        ];
    }
}
