<?php

namespace App\Providers;

use App\Contracts\Repositories\LoanRepositoryInterface;
use App\Contracts\Repositories\RepaymentRepositoryInterface;
use App\Contracts\Repositories\UserRepositoryInterface;
use App\Repositories\LoanRepository;
use App\Repositories\RepaymentRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Dusterio\LumenPassport\LumenPassport;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public function boot()
    {
        LumenPassport::routes($this->app);
        Passport::tokensExpireIn(Carbon::now()->addDays(15));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
        LumenPassport::tokensExpireIn(Carbon::now()->addYears(50), 2);
        LumenPassport::allowMultipleTokens();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //register repositories interface
        $this->app->singleton(UserRepositoryInterface::class, UserRepository::class);
        $this->app->singleton(LoanRepositoryInterface::class, LoanRepository::class);
        $this->app->singleton(RepaymentRepositoryInterface::class, RepaymentRepository::class);
    }

    public function provides()
    {
        return [
            UserRepositoryInterface::class,
            LoanRepositoryInterface::class,
            RepaymentRepositoryInterface::class,
        ];
    }
}
