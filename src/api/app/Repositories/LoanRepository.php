<?php

namespace App\Repositories;

use App\Contracts\Repositories\LoanRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Loan;
use App\Validators\LoanValidator;

/**
 * Class LoanRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class LoanRepository extends BaseRepository implements LoanRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Loan::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getAllRecords($parameters = []){
        return $this->model->when(isset($parameters['user_id']), function ($query) use ($parameters) {
                            return $query->where(['user_id' => $parameters['user_id']]);
                        })->paginate($parameters['limit']);

    }

    public function approveLoan($id){
        $loan = $this->find($id);
        // Todo: You need to check the authorize for approve loan

        $loan->approved = true;
        $loan->approved_date = Carbon::now();
        $loan->approved_by  = Auth::id();
        $loan->save();
        return $loan;
    }

    public function updateByRepayment($id, $amount){
        $loan = $this->find($id);
        $loan->amount_paid = $loan->amount_paid + $amount;
        $restAmount = $loan->total_to_pay - $loan->amount_paid;
        if ($restAmount == 0) {
            $status = 'paid';
        } elseif ($restAmount > 0) {
            $status = 'partial';
        } else {
            $status = 'unpaid';
        }
        $loan->status = $status;
        $loan->save();
        return $loan;
    }
}
