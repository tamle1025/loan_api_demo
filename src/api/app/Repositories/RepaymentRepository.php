<?php

namespace App\Repositories;

use App\Contracts\Repositories\RepaymentRepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Repayment;
use App\Validators\RepaymentValidator;

/**
 * Class RepaymentRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RepaymentRepository extends BaseRepository implements RepaymentRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Repayment::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getAllRecords($parameters = []){
        return $this->model->when(isset($parameters['loan_id']), function ($query) use ($parameters) {
            return $query->where(['loan_id' => $parameters['loan_id']]);
        })->paginate($parameters['limit']);
    }
}
