<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->decimal('amount');
            $table->decimal('total_to_pay');
            $table->decimal('amount_paid')->default(0);
            $table->decimal('pay_per_term');
            $table->integer('duration');
            $table->dateTime('date_applied');
            $table->dateTime('date_end');
            $table->boolean('approved')->nullable()->default(false);
            $table->dateTime('approved_date')->nullable();
            $table->unsignedBigInteger('approved_by')->nullable();
            $table->enum('status', ['unpaid','paid','partial'])->default('unpaid');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('approved_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
