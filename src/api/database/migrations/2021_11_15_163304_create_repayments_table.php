<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateRepaymentsTable.
 */
class CreateRepaymentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('repayments', function(Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('loan_id');
            $table->float('amount',8,2);
            $table->text('description')->nullable();
            $table->boolean('approved')->nullable()->default(false);
            $table->unsignedBigInteger('approved_by')->nullable();
            $table->foreign('loan_id')->references('id')->on('loans')->onDelete('cascade');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('repayments');
	}
}
