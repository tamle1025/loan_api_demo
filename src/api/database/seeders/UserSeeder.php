<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'test@yopmail.com',
            'password' => password_hash('123456', PASSWORD_BCRYPT),
            'name' => 'Test'
        ]);
    }
}
