<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('register', [ 'uses' => 'UsersController@register']);
$router->post('login', [ 'uses' => 'UsersController@login']);

$router->group(['middleware' => 'auth'], function () use ( $router){
    $router->group(['prefix' => 'loans'], function () use ($router) {
        $router->get('/', ['as' => 'loans.list', 'uses' => 'LoanController@index']);
        $router->get('/{id}', ['as' => 'loans.detail', 'uses' => 'LoanController@detail']);
        $router->post('/{id}/approve',['as' => 'loans.approve', 'uses' => 'LoanController@approveLoan']);
        $router->post('/', ['as' => 'loans.create', 'uses' => 'LoanController@create']);
        $router->put('/{id}', ['as' => 'loans.update', 'uses' => 'LoanController@update']);
        $router->delete('/{id}', ['as' => 'loans.delete', 'uses' => 'LoanController@delete']);
    });

    $router->group(['prefix' => 'repayments'], function () use ($router) {
        $router->get('/', ['as' => 'repayments.list', 'uses' => 'RepaymentController@index']);
        $router->get('/{id}', ['as' => 'repayments.detail', 'uses' => 'RepaymentController@detail']);
        $router->post('/{id}/approve',['as' => 'repayments.approve', 'uses' => 'RepaymentController@approveRepayment']);
        $router->post('/', ['as' => 'repayments.create', 'uses' => 'RepaymentController@create']);
    });
});
