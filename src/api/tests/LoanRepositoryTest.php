<?php

use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class LoanRepositoryTest extends TestCase
{
    use DatabaseMigrations;
    public function testCanIApproveLoan(){
        $this->loginWithFakeUser();
        $repository = new \App\Repositories\LoanRepository(app());
        $data = [
            "amount" => 1000,
            "total_to_pay" => 1100,
            "duration" => 30,
            "date_applied" => Carbon::now(),
            "date_end" => Carbon::today()->addDays(30),
            "user_id" => 1
        ];

        $data['pay_per_term'] = $data['total_to_pay'] / ceil($data['duration']/7);
        $record = $repository->create($data);
        $this->seeInDatabase('loans', $data);

        $record = $repository->approveLoan($record->id);
        $this->assertTrue($record->approved);
    }


    public function testCanIUpdateLoanByRepayment(){
        $repository = new \App\Repositories\LoanRepository(app());
        $data = [
            "amount" => 1000,
            "total_to_pay" => 1100,
            "duration" => 30,
            "date_applied" => Carbon::now(),
            "date_end" => Carbon::today()->addDays(30),
            "user_id" => 1
        ];

        $data['pay_per_term'] = $data['total_to_pay'] / ceil($data['duration']/7);
        $record = $repository->create($data);
        $this->seeInDatabase('loans', $data);

        $record = $repository->approveLoan($record->id);
        $this->assertTrue($record->approved);

        $record = $repository->updateByRepayment($record->id, '1100');
        $this->assertEquals('paid',$record->status);
    }
}
