<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class LoanTest extends TestCase
{
    use DatabaseMigrations;

    public function testCanIReadAllLoans()
    {
        $this->loginWithFakeUser();

        $response =  $this->get('/loans');
        return $response->seeJsonStructure(['data']);
    }

    public function testCanICreateNewLoan(){
        $this->loginWithFakeUser();
        $data = [
            "amount" => 1000,
            "total_to_pay" => 1100,
            "duration" => 30,
        ];
        $response =  $this->post('/loans',$data);
        $response->seeJsonStructure(['data']);
    }


    public function testCanIRetrieveSpecificLoan(){
        $this->loginWithFakeUser();

        $data = [
            "amount" => 1000,
            "total_to_pay" => 1100,
            "duration" => 30
        ];
        $response =  $this->post('/loans',$data);
        $response->seeJsonStructure(['data']);
        $generatedLoan = $response->response->getOriginalContent();

        $response = $this->get('/loans/' . $generatedLoan['data']['id']);
        $response->seeJsonStructure(['data']);
    }


    public function testCanIUpdateSpecificLoan(){
        $this->loginWithFakeUser();

        $data = [
            "amount" => 1000,
            "total_to_pay" => 1100,
            "duration" => 30
        ];
        $response =  $this->post('/loans',$data);
        $response->seeJsonStructure(['data']);
        $generatedLoan = $response->response->getOriginalContent();
        $data = $generatedLoan['data'];
        $data['amount'] = 1001;

        $response->put('/loans/'. $data['id'], $data);
        return $response->seeInDatabase('loans', $data);
    }

    public function testCanIDeleteLoan(){
        $this->loginWithFakeUser();

        $data = [
            "amount" => 1000,
            "total_to_pay" => 1100,
            "duration" => 30
        ];
        $response =  $this->post('/loans',$data);
        $response->seeJsonStructure(['data']);
        $generatedLoan = $response->response->getOriginalContent();

        $this->delete('/loans/' . $generatedLoan['data']['id']);
        $response->assertResponseOk();

        $this->notSeeInDatabase('loans', ['id' => $generatedLoan['data']['id']]);
    }

    public function testCanIApproveLoan(){
        $this->loginWithFakeUser();

        $data = [
            "amount" => 1000,
            "total_to_pay" => 1100,
            "duration" => 30
        ];
        $response =  $this->post('/loans',$data);
        $response->seeJsonStructure(['data']);
        $generatedLoan = $response->response->getOriginalContent();

        $this->post('/loans/' . $generatedLoan['data']['id']. '/approve');
        $response->assertResponseOk();
    }
}
