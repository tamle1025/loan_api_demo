<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class RepaymentTest extends TestCase
{
    use DatabaseMigrations;

    public function testCanIReadAllRepayment()
    {
        $this->loginWithFakeUser();
        $response =  $this->get('/repayments');
        return $response->seeJsonStructure(['data']);
    }

    public function testCanICreateNewLoan(){
        $this->loginWithFakeUser();
        $data = [
            "amount" => 1000,
            "total_to_pay" => 1100,
            "duration" => 30
        ];
        $response =  $this->post('/loans',$data);
        $response->seeJsonStructure(['data']);
        $generatedLoan = $response->response->getOriginalContent();

        $this->post('/loans/' . $generatedLoan['data']['id']. '/approve');
        $response->assertResponseOk();

        $data = [
            "amount" => 220,
            "loan_id" => $generatedLoan['data']['id']
        ];
        $response =  $this->post('/repayments',$data);
        $response->seeJsonStructure(['data']);
    }


    public function testCanIRetrieveSpecificRepayment(){
        $this->loginWithFakeUser();
        $data = [
            "amount" => 1000,
            "total_to_pay" => 1100,
            "duration" => 30
        ];
        $response =  $this->post('/loans',$data);
        $response->seeJsonStructure(['data']);
        $generatedLoan = $response->response->getOriginalContent();

        $this->post('/loans/' . $generatedLoan['data']['id']. '/approve');
        $response->assertResponseOk();

        $data = [
            "amount" => 220,
            "loan_id" => $generatedLoan['data']['id']
        ];
        $response =  $this->post('/repayments',$data);
        $response->seeJsonStructure(['data']);
        $generatedRepayment = $response->response->getOriginalContent();

        $response = $this->get('/repayments/' . $generatedRepayment['data']['id']);
        $response->seeJsonStructure(['data']);
    }


    public function testCanIApproveRepayment(){
        $this->loginWithFakeUser();
        $data = [
            "amount" => 1000,
            "total_to_pay" => 1100,
            "duration" => 30
        ];
        $response =  $this->post('/loans',$data);
        $response->seeJsonStructure(['data']);
        $generatedLoan = $response->response->getOriginalContent();

        $this->post('/loans/' . $generatedLoan['data']['id']. '/approve');
        $response->assertResponseOk();

        $data = [
            "amount" => 220,
            "loan_id" => $generatedLoan['data']['id']
        ];
        $response =  $this->post('/repayments',$data);
        $response->seeJsonStructure(['data']);
        $generatedRepayment = $response->response->getOriginalContent();

        $response = $this->post('/repayments/' . $generatedRepayment['data']['id'] . '/approve');
        $response->assertResponseOk();
    }
}
