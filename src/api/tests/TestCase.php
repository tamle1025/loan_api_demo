<?php

use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function loginWithFakeUser()
    {
        $user = new \App\Models\User([
            'id' => 1,
            'email' => 'test@yopmail.com',
            'name' => 'Test',
            'password' => \Illuminate\Support\Facades\Hash::make('test')
        ]);

        $this->be($user);
    }
}
