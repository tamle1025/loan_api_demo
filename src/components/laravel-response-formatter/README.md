# Installation

## Lumen

Using composer to install local package via repository (type = path)

#### Prerequisites

You need to install the following:
1. composer
2. Lumen / Laravel

#### Instruction

1. Edit composer.json to add repository
    ```
        "repositories": [{
            "type": "path",
            "url": "./path/to/package/folder",
            "options": {
                "symlink": false
            }
        }]
    ```

2. Run<br /> `composer require cevinio/laravel-response-formatter`

3. Edit app.php, add in<br />`$app->register(Cevinio\ResponseFormatter\Providers\ResponseServiceProvider::class);`
   
4. Edit Handler to user `ResponseFormatter` in render() to handle exceptions
e.g.:
   
```
    public function render($request, Throwable $exception)
    {
        $clazz = get_class($exception);
        switch ($clazz){
            case ValidationException::class:
                return \ResponseFormatter::send($exception, Response::HTTP_BAD_REQUEST, 'validation_error');
            case MethodNotAllowedHttpException::class:
                return \ResponseFormatter::send($exception, Response::HTTP_METHOD_NOT_ALLOWED, 'invalid_request');
            case ModelNotFoundException::class:
            case NotFoundHttpException::class:
                return \ResponseFormatter::send($exception, Response::HTTP_NOT_FOUND, 'content_not_found');
            default:
                return \ResponseFormatter::send($exception, Response::HTTP_INTERNAL_SERVER_ERROR, 'internal_server_error');
    }
```
