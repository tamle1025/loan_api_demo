<?php

namespace Response\ResponseFormatter\Facades;

use Response\ResponseFormatter\Services\ResponseServiceInterface;
use Illuminate\Support\Facades\Facade;

class ResponseFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ResponseServiceInterface::class;
    }
}
