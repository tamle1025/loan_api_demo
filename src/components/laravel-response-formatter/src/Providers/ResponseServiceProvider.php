<?php

namespace Response\ResponseFormatter\Providers;

use Response\ResponseFormatter\Facades\ResponseFacade;
use Response\ResponseFormatter\Services\ResponseService;
use Response\ResponseFormatter\Services\ResponseServiceInterface;
use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(ResponseServiceInterface::class, ResponseService::class);
        $this->app->bind(ResponseFacade::class);

        if (!class_exists('ResponseFormatter')) {
            class_alias(ResponseFacade::class, 'ResponseFormatter');
        }
    }
}
