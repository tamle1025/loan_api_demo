<?php
namespace Response\ResponseFormatter\Services;

use Response\ResponseFormatter\Utils\ValidationUtil;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use JsonSerializable;

class ResponseService implements ResponseServiceInterface
{
    public function send($data, $code = Response::HTTP_OK, $messageCode = [])
    {
        $messageCodes = $messageCode ?? [];
        if (!empty($messageCode) && !is_array($messageCode)) {
            $messageCodes = [$messageCode];
        }

        $result = [
            "messages"  => &$messageCodes,
            "data"      => null,
        ];

        if ($data instanceof ValidationException) {
            $result["messages"] = array_merge($messageCodes, ValidationUtil::getValidationErrors($data->validator));
        } elseif ($data instanceof \Exception || $data instanceof \Throwable) {
            $clazz = new \ReflectionClass(get_class($data));

            $result["messages"][] = [
                "message_code"  => $clazz->getShortName(),
                "message"       => $data->getMessage() ?? "Unknown Error"
            ];

            if (config("app.debug")) {
                $result["traces"] = $this->getExceptionTraces($data);
            }
        } elseif ($data instanceof EloquentModel) {
            $result["data"] = $data->toArray();
        } elseif ($data instanceof Collection) {
            foreach ($data as $dataItem) {
                if ($dataItem instanceof JsonSerializable) {
                    $result["data"][] = $dataItem->toJson();
                }
            }
        } else {
            $result["data"] = $data;
        }

        return response($result, $code);
    }
    private function getExceptionTraces($exception)
    {
        $traces = $exception->getTraceAsString();
        $traces = preg_split("/#\d+\s+/", $traces, $limit = -1, PREG_SPLIT_NO_EMPTY);
        $traces = array_chunk($traces, 20)[0];
        return $traces;
    }
}
