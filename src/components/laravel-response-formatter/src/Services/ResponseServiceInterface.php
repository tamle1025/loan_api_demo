<?php
namespace Response\ResponseFormatter\Services;

use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;

interface ResponseServiceInterface
{
    public function send($data, $code = Response::HTTP_OK, $messageCode = []);
}
