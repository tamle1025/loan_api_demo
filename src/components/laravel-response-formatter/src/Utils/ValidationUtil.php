<?php

namespace Response\ResponseFormatter\Utils;

use Illuminate\Validation\Validator;

class ValidationUtil
{
    public static function getValidationErrors(Validator $validator)
    {
        $prefixCode = "validation";
        $errors = [];
        $messages = $validator->errors()->getMessages();
        $failedRules = $validator->failed();
        foreach ($failedRules as $field => $rules) {
            $keyRules = array_keys($rules);
            foreach ($keyRules as $index => $keyRule) {
                $keyRule = $keyRules[$index];
                array_push($errors, [
                    "message_code"  => implode("_", [$prefixCode, $field, strtolower($keyRule)]),
                    "message"       => $messages[$field][$index]
                ]);
            }
        }
        return $errors;
    }
}
